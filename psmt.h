/****************************************************************************
 * psmt.h
 *
 *   Copyright (C) 2019 Matias Nitsche. All rights reserved.
 *   Author: Matias Nitsche <mnitsche@dc.uba.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PSMT nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __PSMT_H__
#define __PSMT_H__

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/types.h>
#include <stdbool.h>
#include <psmtdev/psmt_dev.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define PSMT_INVALID_SUB { .fd = -1 }
#define PSMT_INVALID_PUB { .fd = -1 }

/****************************************************************************
 * Type Definitions
 ****************************************************************************/

/*
 * Instance associated with a subscription
 */

struct psmt_subscriber_s
{
  int fd;
};

typedef struct psmt_subscriber_s psmt_subscriber_t;

/*
 * Instance associated with an advertisement
 */

struct psmt_publisher_s
{
  int fd;
};

typedef struct psmt_publisher_s psmt_publisher_t;

/*
 * Datatype to manage dynamic number of pollfd instances
 */

struct psmt_pollset_s
{
  struct pollfd* fds;
  size_t nfds;
  size_t freeslot;
};

typedef struct psmt_pollset_s psmt_pollset_t;

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Name: psmt_subscribe
 *
 * Description:
 *   Subscribe to the given topic. The topic need not be advertised already
 *   (in which case it will be created using the supplied parameters).
 *
 * Input Parameters:
 *   sub - Pointer to a subscription struct instance, which will be modified
 *     to hold the appropriate file descriptor.
 *   topic_name - The topic, which must be an absolute path-like string.
 *   message_size - The size of messages to be sent on this subscription. Note
 *     that this must match to any other subscription/advertisement
 *     already made on this topic.
 *   queue_size - Number of messages for input queue
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int psmt_subscribe(psmt_subscriber_t *sub, const char* topic_name,
                   size_t message_size, size_t queue_size);

/****************************************************************************
 * Name: psmt_subscribe_existing
 *
 * Description:
 *   Subscribe to an existing topic. This allows not having to specify
 *   the message size in advance (which is typically known by the subscriber).
 *   The message size is in turn returned to the caller. If the topic is not
 *   yet advertised, this function will fail.
 *
 * Input Parameters:
 *   sub - Pointer to a subscription struct instance, which will be modified
 *     to hold the appropriate file descriptor.
 *   topic_name - The topic, which must be an absolute path-like string.
 *   message_size - Pointer where to store the topic's message size. NULL
 *     indicates this value is not expected.
 *   queue_size - Number of messages for input queue
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure:
 *
 *   - ENOENT: topic is not yet advertised
 *
 ****************************************************************************/

int psmt_subscribe_existing(psmt_subscriber_t *sub, const char* topic_name,
                            size_t *message_size, size_t queue_size);

/****************************************************************************
 * Name: psmt_subscribe
 *
 * Description:
 *   Unsubscribe to the given topic. This closes the underlying file descriptor.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

void psmt_unsubscribe(psmt_subscriber_t* sub);

/****************************************************************************
 * Name: psmt_advertise
 *
 * Description:
 *   Advertise the given topic. It is valid to have multiple publishers
 *   so it is acceptable to advertise multiple times, since in essence this
 *   simply opens the underlying device for writing.
 *
 * Input Parameters:
 *   pub - Pointer to a advertisement struct instance, which will be modified
 *     to hold the appropriate file descriptor.
 *   topic_name - The topic, which must be an absolute path-like string.
 *   message_size - The size of messages to be sent on this subscription. Note
 *     that this must match to any other subscription/advertisement alredy made
 *     on this topic.
 *   queue_size - The size of the outgoing queue. If set to zero, advertising
 *     is made on blocking-mode. Otherwise a work queue is used to schedule
 *     transfer of messages from outgoing queue to incoming queue of all
 *     subscribers.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure:
 *
 *   - EINVAL: message_size does not match that of existing topic
 *
 ****************************************************************************/

int psmt_advertise(psmt_publisher_t *pub, const char* topic_name,
                   size_t message_size, size_t queue_size);

/****************************************************************************
 * Name: psmt_unadvertise
 *
 * Description:
 *   Stop advertising on this topic, which mostly implies closing the device.
 *   Does not affect any other advertisements.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

void psmt_unadvertise(psmt_publisher_t* pub);

/****************************************************************************
 * Name: psmt_topic_info
 *
 * Description:
 *   Return topic information for existing topic
 *
 * Input Parameters:
 *   tinfo - The topic info
 *   psinfo - Optional pointer to publisher/subscriber info for this topic.
 *     NOTE: this function will allocate memory for pub/sub info lists.
 *     The user is responsible for freeing this memory with
 *     psmt_topic_info_destroy.
 *
 * Returned Value:
 *   Zero (OK) is returned on success. Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int psmt_topic_info(const char* topic_name, struct psmt_topic_info_s *tinfo,
                    struct psmt_pubsub_info_list_s *psinfo);

/****************************************************************************
 * Name: psmt_topic_info_destroy
 *
 * Description:
 *   Destroy allocated publisher/subscriber info
 *
 * Input Parameters:
 *   psinfo - Pointer to publisher/subscriber info
 *
 * Returned Value:
 *   Zero (OK) is returned on success. Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

void psmt_topic_info_destroy(struct psmt_pubsub_info_list_s *psinfo);

/****************************************************************************
 * Name: psmt_sub_set_queue_size
 *
 * Description:
 *   Set the queue size for the subscriber
 *
 * Input Parameters:
 *   queue_size - Queue size in number of messages to set to queue (> 0)
 *
 * Returned Value:
 *   Zero (OK) is returned on success. Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int psmt_sub_set_queue_size(psmt_subscriber_t* sub, size_t queue_size);

/****************************************************************************
 * Name: psmt_topic_lossless
 *
 * Description:
 *   Enable/disable lossless mode on topic. A topic is normally lossy,
 *   meaning that when a subscriber's queue is full, further messages
 *   will be added by dropping older ones in the queue. When lossless mode
 *   is on, the publisher will block waiting for available space for
 *   every subscriber whose queue is full.
 *
 * Input Parameters:
 *   enable - Enable/disable lossless mode
 *
 * Returned Value:
 *   Zero (OK) is returned on success. Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int psmt_topic_lossless(const char* topic, bool enable);

/****************************************************************************
 * Name: psmt_publish
 *
 * Description:
 *   Publish a message to the given topic. This function does not block, except
 *   for excluding mutual access to the device during read/write operations.
 *
 * Input Parameters:
 *   pub - Pointer to a advertisement struct instance.
 *   message - Pointer to the message to publish.
 *   message_size - The size of the message to be sent on this subscription. Note
 *     that this must match to any other subscription/advertisement alredy made
 *     on this topic.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int psmt_publish(psmt_publisher_t* pub, void* message, size_t message_size);

/****************************************************************************
 * Name: psmt_receive
 *
 * Description:
 *   Receive the last unread message. Can also be used to simply mark as read
 *   and discard the message, if desired.
 *
 * Input Parameters:
 *   sub - Pointer to a subscriber struct instance.
 *   message - Pointer to the buffer where to store the message.
 *   message_size - The size of the message. Note that this must match to
 *     any other subscription/advertisement alredy made on this topic.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 *   EWOULDBLOCK - Indicates that the last message was already read.
 *
 ****************************************************************************/

int psmt_receive(psmt_subscriber_t* sub, void* message, size_t message_size);

/****************************************************************************
 * Name: psmt_pollset_create
 *
 * Description:
 *   Initialize pollset structure by allocating memory to hold the given
 * number of file descriptors which will be polled.
 *
 * Input Parameters:
 *   pollset - Pointer to the instance to be initialized
 *   size - The number of file descriptors which will be polled
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 *   ENOMEM - Indicates that no memory is available to allocate the object
 *
 ****************************************************************************/

int psmt_pollset_create(psmt_pollset_t* pollset, size_t size);

/****************************************************************************
 * Name: psmt_pollset_destroy
 *
 * Description:
 *   Deinitialize pollset structure by deallocating the corresponding memory
 *
 * Input Parameters:
 *   pollset - Pointer to the instance to be deinitialized
 *
 ****************************************************************************/

void psmt_pollset_destroy(psmt_pollset_t* pollset);

/****************************************************************************
 * Name: psmt_pollset_addsub
 *
 * Description:
 *   Register a subscriber to the pollset by storing the corresponding
 * file descriptor in the set.
 *
 * Input Parameters:
 *   sub - Pointer to the subscriber instance
 *   pollset - Pointer to the pollset instance
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 *   ENOMEM - Indicates that the number of available slots in the set was
 *     exceeded.
 *
 ****************************************************************************/

int psmt_pollset_addsub(psmt_subscriber_t* sub, psmt_pollset_t* pollset);

/****************************************************************************
 * Name: psmt_pollset_addfd
 *
 * Description:
 *   Directly add a file descriptor to the pollset. Useful por polling
 * devices for reading. The descriptor will be polled for POLLIN.
 *
 * Input Parameters:
 *   fd - File descriptor
 *   pollset - Pointer to the pollset instance
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 *   ENOMEM - Indicates that the number of available slots in the set was
 *     exceeded.
 *
 ****************************************************************************/

int psmt_pollset_addfd(int fd, psmt_pollset_t* pollset);

/****************************************************************************
 * Name: psmt_poll
 *
 * Description:
 *   Poll descriptors in the given pollset. This is mostly a convenience
 * function around poll(). The arguments and return value follow that of
 * poll().
 *
 * Input Parameters:
 *   pollset - Pointer to the pollset instance
 *   timeout - Number of milliseconds to wait (0: do not wait, -1: forever)
 *
 * Returned Value:
 *   The return value follows that of poll(), where the number of ready
 *   subscribers/file descriptors is returned an zero indicates no FD ready
 *   (timeout). In contrast to poll(), on errors a negative errno value
 *   is directly returned.
 *
 ****************************************************************************/

int psmt_poll(psmt_pollset_t* pollset, int timeout);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __PSMT_H__
