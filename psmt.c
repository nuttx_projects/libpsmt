/****************************************************************************
 * psmt.c
 *
 *   Copyright (C) 2019 Matias Nitsche. All rights reserved.
 *   Author: Matias Nitsche <mnitsche@dc.uba.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PSMT nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include <debug.h>
#include <poll.h>
#include <getopt.h>
#include <signal.h>
#include <sys/time.h>

#include "psmt.h"

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct psmt_cmds_s
{
  const char *cmd;
  int (*handler)(int argc, char **argv);
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int cmd_hz(int argc, char **argv);
static int cmd_echo(int argc, char **argv);
static int cmd_info(int argc, char **argv);

static void on_interrupt(int signo);

/****************************************************************************
 * Private Data
 ****************************************************************************/

struct psmt_cmds_s g_cmds[] =
{
  { "hz",   cmd_hz },
  { "echo", cmd_echo },
  { "info", cmd_info },
};

bool g_should_stop = false;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void on_interrupt(int signo)
{
  g_should_stop = true;
}

static void print_pubsub_info(struct psmt_pubsub_info_s *info)
{
  const char process_name[CONFIG_TASK_NAME_SIZE];
  pthread_getname_np(info->pid, process_name);

  size_t total_count = info->transfered_count + info->dropped_count;
  size_t loss_rate = (total_count == 0 ? 0 : (info->dropped_count / total_count) * 100);

  printf("  %s[%i]:\t%lu\t%lu\t%lu%%\n",
         process_name, info->pid, info->dropped_count,
         info->transfered_count, loss_rate);
}

static int cmd_info(int argc, char **argv)
{
  const char *topic;
  struct psmt_topic_info_s tinfo;
  struct psmt_pubsub_info_list_s psinfo;
  int ret;
  int i;

  if (argc < 2 || strcmp(argv[1], "-h") == 0)
    {
      fprintf(stderr, "Usage: psmt info <topic>\n");
      return ERROR;
    }

  psmt_publisher_t pub;
  psmt_advertise(&pub, "/asd", 1, 0);

  psmt_subscriber_t sub1, sub2;
  psmt_subscribe(&sub1, "/asd", 1, 1);
  psmt_subscribe(&sub2, "/asd", 1, 2);

  /* get info */

  topic = argv[1];

  ret = psmt_topic_info(topic, &tinfo, &psinfo);

  if (ret < 0)
    {
      fprintf(stderr, "ERROR: could not get topic info for %s: %s\n",
              topic, strerror(-ret));
      goto errout;
    }

  /* print info */

  printf("Num. subscribers:\t%lu\n"
         "Num. publishers:\t%lu\n"
         "Msg. size:\t\t%lu\n"
         "Lossless?\t\t%s\n",
         tinfo.num_subscribers, tinfo.num_publishers,
         tinfo.message_size, tinfo.lossless ? "yes" : "no");

  printf("\nSubscribers:\n");
  printf("  process\tdropped\txfered\tloss\n");
  for (i = 0; i < psinfo.sub_info_size; i++)
    {
      print_pubsub_info(&psinfo.sub_info[i]);
    }

  printf("\nPublishers:\n");
  printf("  process\tdropped\txfered\tloss\n");
  for (i = 0; i < psinfo.pub_info_size; i++)
    {
      print_pubsub_info(&psinfo.pub_info[i]);
    }

errout:
  psmt_topic_info_destroy(&psinfo);
  return ret;
}

static int cmd_hz(int argc, char **argv)
{
  int ret;
  const char *topic;
  psmt_subscriber_t sub;
  struct pollfd fds;
  timer_t t;
  struct sigevent evp;
  struct itimerspec timeout;
  size_t msg_count = 0;
#if 0
  size_t min_msg_count = SIZE_MAX;
  size_t max_msg_count = 0;
  size_t avg_msg_count = 0;
#endif

  if (argc < 2 || strcmp(argv[1], "-h") == 0)
    {
      fprintf(stderr, "Usage: psmt hz <topic>\n");
      return ERROR;
    }

  /* subscribe */

  topic = argv[1];

  ret = psmt_subscribe_existing(&sub, topic, NULL, 1);

  if (ret < 0)
    {
      fprintf(stderr, "Could not open topic %s: %s\n", topic, strerror(-ret));
      return ret;
    }

  printf("Subscribed to %s\n", topic);

  /* poll for messages until interrupted */

  evp.sigev_notify = SIGEV_SIGNAL;
  evp.sigev_signo = SIGUSR1;
  timer_create(CLOCK_REALTIME, &evp, &t);

  timeout.it_interval.tv_sec = 1;
  timeout.it_interval.tv_nsec = 0;
  timeout.it_value = timeout.it_interval;
  timer_settime(&t, 0, &timeout, NULL);

  fds.fd = sub.fd;
  fds.events = POLLIN;

  while (!g_should_stop)
    {
      if (poll(&fds, 1, -1) <= 0)
        {
          if (errno == EINTR && !g_should_stop)
            {
              printf("count: %lu\n", msg_count);
              msg_count = 0;
            }
          else
            {
              break;
            }
        }
      else
        {
          msg_count++;
        }
    }

  /* teardown */

  timer_delete(&t);
  psmt_unsubscribe(&sub);

  return OK;
}

static int cmd_echo(int argc, char **argv)
{
  int opt;
  bool print_help = false;
  psmt_subscriber_t sub;
  const char *topic;
  size_t message_size;
  int ret;
  struct pollfd fds;

  while ((opt = getopt(argc, argv, "h")) != -1)
    {
      switch(opt)
        {
          case 'h':
          default:
            print_help = true;
            break;
        }
    }

  if (print_help || optind >= argc)
    {
      goto cmd_echo_help;
    }

  /* subscribe to to topic */

  topic = argv[optind];

  if (optind == argc - 1)
    {
      /* no message size provided, assume topic already advertised and
       * obtain message size
       */

      ret = psmt_subscribe_existing(&sub, topic, &message_size, 1);
    }
  else
    {
      /* Do not assume anything, pass expected message size (which will
       * be checked against existing topic, in such case)
       */

      message_size = atol(argv[argc - 2]);

      ret = psmt_subscribe(&sub, topic, message_size, 1);
    }

  if (ret < 0)
    {
      fprintf(stderr, "ERROR: could not subscribe to topic: %s\n", strerror(-ret));
      return ERROR;
    }

  fds.fd = sub.fd;
  fds.events = POLLIN;

  /* poll for messages until interrupted */

  while (poll(&fds, 1, -1) >= 0 && !g_should_stop)
    {
      printf(".");
    }

  printf("\n");

  /* teardown */

  psmt_unsubscribe(&sub);

  return OK;

cmd_echo_help:
  fprintf(stderr, "Usage: psmt echo <topic>\n");
  return ERROR;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: pubsub_main
 ****************************************************************************/

int psmt_main(int argc, FAR char *argv[])
{
  int ret = ERROR;
  int i;
  struct sigaction act;

  if (argc <= 1 || strcmp(argv[1], "-h") == 0)
    {
      fprintf(stderr,
              "Usage: psmt <command> <options>\n"
              "Commands:\n"
              "\thz       Measure message rate of topic\n"
              "\techo     Print messages sent to topic\n"
              "\tinfo     Print topic information\n");

      return ERROR;
    }

  /* register interrupt signal */

  act.sa_u._sa_handler = on_interrupt;
  act.sa_mask = 0;
  act.sa_flags = 0;
  sigaction(SIGINT, &act, NULL);

  /* process commands */

  for (i = 0; i < sizeof(g_cmds) / sizeof(struct psmt_cmds_s); i++)
    {
      if (strcmp(argv[1], g_cmds[i].cmd) == 0)
        {
          ret = g_cmds[i].handler(argc - 1, argv + 1);

          if (ret < 0)
            {
              return ret;
            }

          break;
        }
    }

  if (ret == ERROR)
    {
      fprintf(stderr, "Uknown command \"%s\"\n", argv[1]);
    }

  return ret;
}
