/****************************************************************************
 * /home/v01d/coding/nuttx_latest/extra_apps/libpsmt/psmt_test.c
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "psmt.h"
#include <testing/unity.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void test_pollset(void)
{
  int i;
  psmt_pollset_t ps;

  /* verify pollset create */

  TEST_ASSERT_EQUAL_INT(0, psmt_pollset_create(&ps, 5));

  /* fill up pollset with fds */

  for (i = 0; i < 5; i++)
    {
      TEST_ASSERT_EQUAL_INT(0, psmt_pollset_addfd(i, &ps));
    }

  /* verify pollset filled up */

  TEST_ASSERT_EQUAL_INT(-ENOMEM, psmt_pollset_addfd(i, &ps));

  psmt_pollset_destroy(&ps);
}

static void test_subscribe(void)
{
  psmt_subscriber_t sub1, sub2, sub3, sub4, sub5;
  size_t message_size;

  /* Open /topic1 (twice) and /topic2 (once), with different queue sizes */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub1, "/topic1", 1, 1));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub2, "/topic2", 1, 2));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub3, "/topic1", 1, 3));

  /* Subscribe to existing /topic1 */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe_existing(&sub4, "/topic1", &message_size, 1));
  TEST_ASSERT_EQUAL_UINT(1, message_size);

  /* Subscribe to non-existing /topic3 (assuming it exists) */

  TEST_ASSERT_EQUAL_INT(-ENOENT, psmt_subscribe_existing(&sub5, "/topic3", NULL, 1));

  /* Unsubscribe all topics */

  psmt_unsubscribe(&sub1);
  psmt_unsubscribe(&sub2);
  psmt_unsubscribe(&sub3);
  psmt_unsubscribe(&sub4);
}

static void test_advertise(void)
{
  psmt_publisher_t pub1, pub2, pub3, pub4;

  /* Attempt to create publisher with invalid queue size */
  /* TODO: this will eventually be supported and fail */

  TEST_ASSERT_EQUAL(-EPERM, psmt_advertise(&pub1, "/topic1", 1, 1));

  /* Open /topic1 (twice) and /topic2 (once) */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub1, "/topic1", 1, 0));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub2, "/topic2", 1, 0));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub3, "/topic1", 1, 0));

  /* Open with incorrect message_size */

  TEST_ASSERT_EQUAL_INT(-EINVAL, psmt_advertise(&pub4, "/topic1", 2, 0));

  /* Unadvertise */

  psmt_unadvertise(&pub1);
  psmt_unadvertise(&pub2);
  psmt_unadvertise(&pub3);
}

static void *send_thread(void *arg)
{
  psmt_publisher_t *pub = (psmt_publisher_t *)arg;
  uint8_t msg;
  int ret;

  msg = 1;
  ret = psmt_publish(pub, &msg, 1);

  if (ret < 0)
    {
      return (void *)1;
    }

  msg = 2;
  ret = psmt_publish(pub, &msg, 1);

  if (ret < 0)
    {
      return (void *)2;
    }

  return NULL;
}

static void test_pubsub(void)
{
  psmt_subscriber_t sub1, sub2, sub3;
  psmt_publisher_t pub1, pub2, pub3;
  uint8_t msg1, msg2;
  pthread_t thread;
  pthread_addr_t retval;

  /* Subscribe to /topic1 (twice) and /topic2 (once) */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub1, "/topic1", 1, 1));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub2, "/topic1", 1, 1));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub3, "/topic2", 1, 1));

  /* Verify nothing is received */

  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub3, &msg1, 1));

  /* Verify wrong message size detected */

  TEST_ASSERT_EQUAL(-EINVAL, psmt_receive(&sub1, &msg1, 2));
  TEST_ASSERT_EQUAL(-EINVAL, psmt_receive(&sub2, &msg1, 2));
  TEST_ASSERT_EQUAL(-EINVAL, psmt_receive(&sub3, &msg1, 2));

  /* Advertise /topic1 (twice) and /topic2 (once) */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub1, "/topic1", 1, 0));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub2, "/topic1", 1, 0));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub3, "/topic2", 1, 0));

  /* Publish "1" and then "2" on /topic1 (via different publishers),
   * and only "1" on /topic2.
   */

  msg1 = 1;
  msg2 = 2;

  TEST_ASSERT_EQUAL(0, psmt_publish(&pub1, &msg1, 1));
  TEST_ASSERT_EQUAL(0, psmt_publish(&pub2, &msg2, 1));
  TEST_ASSERT_EQUAL(0, psmt_publish(&pub3, &msg1, 1));

  /* Verify most recent message received on each topic */

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(2, msg1);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(2, msg1);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub3, &msg1, 1));
  TEST_ASSERT_EQUAL(1, msg1);

  /* Verify nothing else to receive */

  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub3, &msg1, 1));

  /* Enable lossless mode on /topic1 */

  TEST_ASSERT_EQUAL(0, psmt_topic_lossless("/topic1", true));

  /* Send two messages in separate thread, should block until
   * first message is received on both subscribers
   */

  pthread_create(&thread, NULL, send_thread, &pub1);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(1, msg1);
  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(1, msg1);

  pthread_join(thread, &retval);
  TEST_ASSERT_EQUAL(0, retval);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(2, msg1);
  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(2, msg1);

  /* Disable lossless mode on /topic1 */

  TEST_ASSERT_EQUAL(0, psmt_topic_lossless("/topic1", false));

  /* Increase queue size in subscribers */

  TEST_ASSERT_EQUAL(0, psmt_sub_set_queue_size(&sub1, 3));
  TEST_ASSERT_EQUAL(0, psmt_sub_set_queue_size(&sub2, 3));
  TEST_ASSERT_EQUAL(0, psmt_sub_set_queue_size(&sub3, 3));

  /* Publish same thing again */

  msg1 = 1;
  msg2 = 2;

  TEST_ASSERT_EQUAL(0, psmt_publish(&pub1, &msg1, 1));
  TEST_ASSERT_EQUAL(0, psmt_publish(&pub2, &msg2, 1));
  TEST_ASSERT_EQUAL(0, psmt_publish(&pub3, &msg1, 1));

  /* Tear down advertisers early (no longer needed) */


  psmt_unadvertise(&pub1);
  psmt_unadvertise(&pub2);
  psmt_unadvertise(&pub3);

  /* Verify both messages received on /topic1, on both subscribers */

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(1, msg1);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(2, msg1);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(1, msg1);

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(2, msg1);

  /* Still only "1" received on /topic2 */

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub3, &msg1, 1));
  TEST_ASSERT_EQUAL(1, msg1);

  /* Verify nothing else to receive */

  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub1, &msg1, 1));
  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub2, &msg1, 1));
  TEST_ASSERT_EQUAL(-EWOULDBLOCK, psmt_receive(&sub3, &msg1, 1));

  /* Teardown subscribers */

  psmt_unsubscribe(&sub1);
  psmt_unsubscribe(&sub2);
  psmt_unsubscribe(&sub3);
}

static void test_poll(void)
{
  psmt_subscriber_t sub1, sub2;
  psmt_publisher_t pub;
  uint8_t msg;
  psmt_pollset_t pollset;

  /* Subscribe to /topic1 */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub1, "/topic1", 1, 3));
  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_subscribe(&sub2, "/topic1", 1, 3));

  /* Advertise /topic1 */

  TEST_ASSERT_GREATER_OR_EQUAL(0, psmt_advertise(&pub, "/topic1", 1, 0));

  /* Create pollset */

  TEST_ASSERT_EQUAL(0, psmt_pollset_create(&pollset, 2));
  TEST_ASSERT_EQUAL(0, psmt_pollset_addsub(&sub1, &pollset));
  TEST_ASSERT_EQUAL(0, psmt_pollset_addsub(&sub2, &pollset));

  /* Poll and verify no pending event */

  TEST_ASSERT_EQUAL(0, psmt_poll(&pollset, 0));

  /* Publish two messages */

  msg = 1;
  TEST_ASSERT_EQUAL(0, psmt_publish(&pub, &msg, 1));
  msg = 2;
  TEST_ASSERT_EQUAL(0, psmt_publish(&pub, &msg, 1));

  /* Poll once and verify message are received */

  TEST_ASSERT_EQUAL(2, psmt_poll(&pollset, 0));

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg, 1));
  TEST_ASSERT_EQUAL(1, msg);
  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg, 1));
  TEST_ASSERT_EQUAL(1, msg);

  /* Poll again and verify message are received */

  TEST_ASSERT_EQUAL(2, psmt_poll(&pollset, 0));

  TEST_ASSERT_EQUAL(0, psmt_receive(&sub1, &msg, 1));
  TEST_ASSERT_EQUAL(2, msg);
  TEST_ASSERT_EQUAL(0, psmt_receive(&sub2, &msg, 1));
  TEST_ASSERT_EQUAL(2, msg);

  /* Poll once more and no available messages should be reported */

  TEST_ASSERT_EQUAL(0, psmt_poll(&pollset, 0));

  /* Teardown */

  psmt_unadvertise(&pub);
  psmt_unsubscribe(&sub1);
  psmt_unsubscribe(&sub2);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

int psmt_test_main(int argc, FAR char *argv[])
{
  UNITY_BEGIN();

  RUN_TEST(test_pollset);
  RUN_TEST(test_subscribe);
  RUN_TEST(test_advertise);
  RUN_TEST(test_pubsub);
  RUN_TEST(test_poll);

  return UNITY_END();
}

