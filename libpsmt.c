/****************************************************************************
 * libpsmt.c
 *
 *   Copyright (C) 2019 Matias Nitsche. All rights reserved.
 *   Author: Matias Nitsche <mnitsche@dc.uba.ar>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PSMT nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/ioctl.h>
#include "psmt.h"

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int psmt_register_topic(const char* topic_name, size_t message_size);

static int psmt_open_topic(const char* topic_name, size_t message_size,
                           size_t queue_size, bool subscribe);

static int psmt_open_existing_topic(const char* topic_name,
                                    size_t *message_size, size_t queue_size,
                                    bool subscribe);

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int psmt_register_topic(const char* topic_name, size_t message_size)
{
  int ret;
  int fd;
  struct psmt_topic_config_t topic_config;

  fd = open(CONFIG_PSMT_DEV_PATH, O_NONBLOCK);

  if (fd >= 0)
    {
      strncpy(topic_config.topic_name, topic_name, PATH_MAX);
      topic_config.message_size = message_size;

      /* attempt to create topic (this may fail if already registered) */

      ret = ioctl(fd, 0, (unsigned long)&topic_config); // TODO: replace with apropriate ioctl number

      close(fd);
    }
  else
    {
      ret = -errno;
    }

  return ret;
}

static int psmt_open_existing_topic(const char* topic_name,
                                    size_t *message_size, size_t queue_size,
                                    bool subscribe)
{
  char path[PATH_MAX];
  int ret;
  bool use_queue = (subscribe || queue_size != 0);

  if (subscribe && queue_size == 0)
    {
      /* subscriber cannot have zero queue */

      return -EINVAL;
    }

  /* First attempt to open topic, assuming it already exists. For publisher
   * the device is opened in non-blocking mode if the queue is used.
   */

  snprintf(path, PATH_MAX, CONFIG_PSMT_DEV_PREFIX "%s", topic_name);
  ret = open(path, (use_queue ? O_NONBLOCK : 0) |
                   (subscribe ? O_RDONLY : O_WRONLY));

  /* topic opened, get message size if expected by caller */

  if (ret >= 0 && message_size)
    {
      struct psmt_topic_info_s tinfo;
      ioctl(ret, IOCTL_PSMT_TOPIC_INFO, &tinfo);
      *message_size = tinfo.message_size;
    }

  /* set queue size */

  if (ret >= 0 && use_queue)
    {
      /* set queue size */

      if (ioctl(ret, IOCTL_PSMT_SET_QUEUE_SIZE, &queue_size) < 0)
        {
          close(ret);
          ret = ERROR;
        }
    }

  if (ret < 0)
    {
      ret = -errno;
    }

  return ret;
}

static int psmt_open_topic(const char* topic_name, size_t message_size,
                           size_t queue_size, bool subscribe)
{
  int ret;
  size_t actual_message_size = message_size;

  /* first attempt to open topic, assuming it already exists */

  ret = psmt_open_existing_topic(topic_name, &actual_message_size,
                                 queue_size, subscribe);

  /* it doesn't so create it */

  if (ret < 0 && errno == ENOENT)
    {
      ret = psmt_register_topic(topic_name, message_size);

      if (ret < 0 && ret == -EEXIST)
        {
          /* Someone else beat us to creating the topic, open and request
           * message size.
           */

          ret = psmt_open_existing_topic(topic_name, &actual_message_size,
                                         queue_size, subscribe);
        }
      else if (ret >= 0)
        {
          /* We registered the topic, open it */

          ret = psmt_open_existing_topic(topic_name, NULL, queue_size,
                                         subscribe);

          actual_message_size = message_size;
        }
    }

  /* Topic is open (we either created it ourselves or it was
   * already advertised). Verify expted message size matches the one
   * on the topic.
   */

  if (ret >= 0)
    {
      if (message_size != actual_message_size)
        {
          close(ret);
          ret = -EINVAL;
        }
    }

  /* return any error or open fd */

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: psmt_subscribe
 ****************************************************************************/

int psmt_subscribe(psmt_subscriber_t* sub, const char* topic_name,
                   size_t message_size, size_t queue_size)
{
  assert(sub);
  assert(topic_name);
  assert(message_size > 0);
  assert(queue_size > 0);

  /* create/open topic */

  sub->fd = psmt_open_topic(topic_name, message_size, queue_size, true);

  return sub->fd;
}

/****************************************************************************
 * Name: psmt_subscribe
 ****************************************************************************/

int psmt_subscribe_existing(psmt_subscriber_t *sub, const char* topic_name,
                            size_t *message_size, size_t queue_size)
{
  assert(sub);
  assert(topic_name);
  assert(queue_size > 0);

  sub->fd = psmt_open_existing_topic(topic_name, message_size,
                                     queue_size, true);

  return sub->fd;
}

/****************************************************************************
 * Name: psmt_unsubscribe
 ****************************************************************************/

void psmt_unsubscribe(psmt_subscriber_t* sub)
{
  assert(sub);
  assert(sub->fd >= 0);

  close(sub->fd);

  sub->fd = -1;
}

/****************************************************************************
 * Name: psmt_advertise
 ****************************************************************************/

int psmt_advertise(psmt_publisher_t* pub, const char* topic_name,
                   size_t message_size, size_t queue_size)
{
  assert(pub);
  assert(topic_name);
  assert(message_size > 0);

  pub->fd = psmt_open_topic(topic_name, message_size, queue_size, false);

  return pub->fd;
}

/****************************************************************************
 * Name: psmt_unadvertise
 ****************************************************************************/

void psmt_unadvertise(psmt_publisher_t* pub)
{
  assert(pub);
  assert(pub->fd >= 0);

  close(pub->fd);

  pub->fd = -1;
}

/****************************************************************************
 * Name: psmt_topic_info
 ****************************************************************************/

int psmt_topic_info(const char *topic_name, struct psmt_topic_info_s *tinfo,
                    struct psmt_pubsub_info_list_s *psinfo)
{
  char path[PATH_MAX];
  int ret;
  int fd;

  assert(tinfo);

  if (psinfo)
    {
      memset(psinfo, 0, sizeof(*psinfo));
    }

  /* attempt to open topic, assuming it already exists */

  snprintf(path, PATH_MAX, CONFIG_PSMT_DEV_PREFIX "%s", topic_name);
  fd = open(path, O_NONBLOCK | O_RDONLY);

  if (fd < 0)
    {
      ret = fd;
    }
  else
    {
      ret = ioctl(fd, IOCTL_PSMT_TOPIC_INFO, tinfo);

      if (ret >= 0 && psinfo)
        {
          psinfo->pub_info_size = tinfo->num_publishers;
          psinfo->sub_info_size = tinfo->num_subscribers;

          psinfo->pub_info = malloc(tinfo->num_publishers *
                                    sizeof(struct psmt_pubsub_info_s));

          psinfo->sub_info = malloc(tinfo->num_subscribers *
                                    sizeof(struct psmt_pubsub_info_s));

          ret = ioctl(fd, IOCTL_PSMT_GET_PUBSUB_INFO, psinfo);
        }
    }

  if (ret < 0)
    {
      ret = -errno;
    }

  if (fd >= 0)
    {
      close(fd);
    }

  return ret;
}

/****************************************************************************
 * Name: psmt_topic_info_destroy
 ****************************************************************************/

void psmt_topic_info_destroy(struct psmt_pubsub_info_list_s *psinfo)
{
  assert(psinfo);

  free(psinfo->pub_info);
  free(psinfo->sub_info);
}

/****************************************************************************
 * Name: psmt_sub_set_queue_size
 ****************************************************************************/

int psmt_sub_set_queue_size(psmt_subscriber_t* sub, size_t queue_size)
{
  assert(sub);
  assert(sub->fd >= 0);
  assert(queue_size > 0);

  if (ioctl(sub->fd, IOCTL_PSMT_SET_QUEUE_SIZE, &queue_size) < 0)
    {
      return -errno;
    }
  else
    {
      return OK;
    }
}

/****************************************************************************
 * Name: psmt_topic_lossless
 ***************************************************************************/

int psmt_topic_lossless(const char* topic, bool enable)
{
  char path[PATH_MAX];
  int fd;
  int ret;

  /* attempt to open topic, assuming it already exists */

  snprintf(path, PATH_MAX, CONFIG_PSMT_DEV_PREFIX "%s", topic);
  fd = open(path, O_NONBLOCK | O_RDONLY);

  if (fd < 0)
    {
      ret = -errno;
    }
  else
    {
      ret = ioctl(fd, IOCTL_PSMT_SET_LOSSLESS, enable);

      if (ret < 0)
        {
          ret = -errno;
        }

      close(fd);
    }

  return ret;
}

/****************************************************************************
 * Name: psmt_publish
 ****************************************************************************/

int psmt_publish(psmt_publisher_t* pub, void* message, size_t message_size)
{
  assert(pub);
  assert(pub->fd >= 0);
  assert(message);

  return (write(pub->fd, message, message_size) == message_size ? 0 : -1);
}

/****************************************************************************
 * Name: psmt_receive
 ****************************************************************************/

int psmt_receive(psmt_subscriber_t* sub, void* message, size_t message_size)
{
  int ret;

  assert(sub);
  assert(sub->fd >= 0);

  ret = read(sub->fd, message, message_size);

  /* There should be no partial transfers */

  assert(ret < 0 || ret == message_size);

  if (ret < 0)
    {
      ret = -errno;
    }
  else
    {
      ret = OK;
    }

  return ret;
}

/****************************************************************************
 * Name: psmt_pollset_create
 ****************************************************************************/

int psmt_pollset_create(psmt_pollset_t* pollset, size_t size)
{
  assert(pollset);
  assert(size > 0);

  pollset->fds = (struct pollfd*)malloc(size * sizeof(struct pollfd));

  if (!pollset->fds)
  {
    return -ENOMEM;
  }
  else
  {
    pollset->nfds = size;
    pollset->freeslot = 0;
    return 0;
  }
}

/****************************************************************************
 * Name: psmt_pollset_destroy
 ****************************************************************************/

void psmt_pollset_destroy(psmt_pollset_t* pollset)
{
  assert(pollset);

  if (pollset->fds)
  {
    free(pollset->fds);
    pollset->fds = 0;
  }
}

/****************************************************************************
 * Name: psmt_pollset_addsub
 ****************************************************************************/

int psmt_pollset_addsub(psmt_subscriber_t* sub, psmt_pollset_t* pollset)
{
  assert(sub);
  assert(sub->fd >= 0);

  return psmt_pollset_addfd(sub->fd, pollset);
}

/****************************************************************************
 * Name: psmt_pollset_addfd
 ****************************************************************************/

int psmt_pollset_addfd(int fd, psmt_pollset_t* pollset)
{
  assert(pollset);
  assert(pollset->fds);

  if (pollset->freeslot == pollset->nfds)
  {
    return -ENOMEM;
  }
  else
  {
    pollset->fds[pollset->freeslot].fd = fd;
    pollset->fds[pollset->freeslot].events = POLLIN;
    pollset->freeslot++;
  }

  return 0;
}

/****************************************************************************
 * Name: psmt_poll
 ****************************************************************************/

int psmt_poll(psmt_pollset_t* pollset, int timeout)
{
  assert(pollset);

  return poll(pollset->fds, pollset->nfds, timeout);
}
